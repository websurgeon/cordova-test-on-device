

---------------------------
Minimum Marketable Features
...........................
* config page with default values for test server and port
- display device info on config page to show that phonegap API is available
- page to show test server output
- editable config page details
- ability to stop (unregister device)

---------------------------
User Stories (for * MMF)
...........................
* app starts up on config page
- config details are visible (server & port)


---------------------------
Engineering Tasks
...........................
* ammed dist task to create platform specific app directories:
    - create dist/<platform>/app directories that can be referenced in (or copied to) phonegap apps
    - create dist/phonegapbuild/app.zip that can be uploaded to the PhoneGapBuild site
- create Android phonegap app with specific production app directory (outside of this project)
- create iOS phonegap app with specific production app directory (outside of this project)



---------------------------
TODO
...........................
- maybe automate upload to PhoneGapBuild account



---------------------------
NOTES
...........................
