/* Adapted from code created by James Shore (Copyright (c) 2012) */

/*jslint node: true */

(function (exports) {
    'use strict';

    var jshint = require('jshint').JSHINT,
        fs = require('fs'),
        colors = require('pb.cli.colors');


    exports.validateSource = function (sourceCode, options, globals, description) {
        description = description ? description + ' ' : '';
        var pass = jshint(sourceCode, options, globals);
        if (pass) {
            console.log(description + 'OK');
        } else {
            console.log(colors.red + '------- ' + colors.white + 'FILE FAILED LINTING' + colors.red + ' -------' + colors.reset);
            console.log(colors.green + description + colors.red + 'FAILED' + colors.reset);
            jshint.errors.forEach(function (error) {
                if (error) {
                    if (error.id) {
                        console.log(colors.red + error.line + colors.yellow + ': ' + error.evidence.trim() + colors.reset);
                        console.log(colors.white + '   ' + error.reason + colors.reset);
                    } else {
                        console.log(colors.red + error.line + colors.yellow + ': ' + error.reason + colors.reset);
                    }
                }
            });
            console.log(colors.red + '----------- ' + colors.white + 'END OF FILE' + colors.red + ' -----------' + colors.reset);
        }
        return pass;
    };

    exports.validateFile = function (filename, options, globals) {
        var sourceCode = fs.readFileSync(filename, 'utf8');
        return exports.validateSource(sourceCode, options, globals, filename);
    };

    exports.validateFileList = function (fileList, options, globals) {
        var pass = true;
        fileList.forEach(function (filename) {
            pass = exports.validateFile(filename, options, globals) && pass;
        });
        return pass;
    };

}(exports));