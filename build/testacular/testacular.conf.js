// Testacular configuration
// Generated on Mon Feb 18 2013 21:48:17 GMT+0000 (GMT)


// base path, that will be used to resolve files and exclude
basePath = '../..';


// list of files / patterns to load in the browser
files = [
    MOCHA,
    MOCHA_ADAPTER,
    "node_modules/expect.js/expect.js",
    "app/_index_tests.js"
];


// list of files to exclude
exclude = [

];


// test results reporter to use
// possible values: 'dots', 'progress', 'junit'
reporters = ['progress'];


// web server port
port = 8080;


// cli runner port
runnerPort = 9100;


// enable / disable colors in the output (reporters and logs)
colors = true;


// level of logging
// possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
logLevel = LOG_INFO;


// enable / disable watching file and executing tests whenever any file changes
autoWatch = false;


// Start these browsers, currently available:
// - Chrome
// - ChromeCanary
// - Firefox
// - Opera
// - Safari (only Mac)
// - PhantomJS
// - IE (only Windows)
browsers = [];


// If browser does not capture in given timeout [ms], kill it
captureTimeout = 5000;


// Continuous Integration mode
// if true, it capture browsers, run tests and exit
singleRun = false;

browserNameParser = function (fullName) {
    var BROWSER = /(Chrome|Firefox|Opera|Safari|PhantomJS)\/([0-9]*\.[0-9]*)/,
        VERSION = /Version\/([0-9]*\.[0-9]*)/,
        MSIE = /MSIE ([0-9]*\.[0-9]*)/,
        browserMatch = fullName.match(BROWSER),
        versionMatch,
        ieMatch;
    if (browserMatch) {
        versionMatch = fullName.match(VERSION);
        return browserMatch[1] + ' ' + ((versionMatch && versionMatch[1]) || browserMatch[2]);
    }

    ieMatch = fullName.match(MSIE);
    if (ieMatch) {
        return 'IE ' + ieMatch[1];
    }

    return fullName;
};